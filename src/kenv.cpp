#ifndef KENV_CPP
#define KENV_CPP
#include "kenv.h"
namespace khuma{      
    bool KEnv::a = false;
    KEnv::KEnv(){
        nvs = 26;
        _ENV[0] = "HTTPS";
        _ENV[1] = "HTTP_ACCEPT";
        _ENV[2] = "HTTP_ACCEPT_ENCODING";
        _ENV[3] = "HTTP_ACCEPT_LANGUAGE";
        _ENV[4] = "HTTP_CONNECTION";
        _ENV[5] = "HTTP_HOST";
        _ENV[6] = "HTTP_USER_AGENT";
        _ENV[7] = "QUERY_STRING";
        _ENV[8] = "REQUEST_METHOD";
        _ENV[9] = "CONTENT_TYPE";
        _ENV[10] = "CONTENT_LENGTH";
        _ENV[11] = "GATEWAY_INTERFACE";
        _ENV[12] = "PATH";

        _ENV[13] = "SCRIPT_FILENAME";
        _ENV[14] = "SCRIPT_NAME";
        _ENV[15] = "DOCUMENT_URI";
        _ENV[16] = "DOCUMENT_ROOT";
        _ENV[17] = "REQUEST_URI";
        _ENV[18] = "REQUEST_SCHEME";

        _ENV[19] = "REMOTE_ADDR";
        _ENV[20] = "REMOTE_PORT";
        _ENV[21] = "SERVER_ADDR";
        _ENV[22] = "SERVER_PORT";
        _ENV[23] = "SERVER_NAME";
        _ENV[24] = "SERVER_SOFTWARE";
        _ENV[25] = "SERVER_PROTOCOL";

        const char* name;
        for ( int i = 0; i < nvs; i++ ) {
            name = _ENV[i].c_str();
            char *value = getenv(name);
            if ( value != nullptr ){
                if (_ENV[i] == "QUERY_STRING" && (std::string)(getenv("REQUEST_METHOD")) == "POST"){
                    int ilen = atoi(getenv("CONTENT_LENGTH"));
                    QUERY_STRING = (char*)malloc(ilen+1);
                    fread(QUERY_STRING, ilen, 1, stdin);
                } else {
                    ENV[name] = value;
                }
            } else {
                char a[1] = {' '};
                ENV[name] = a;
            }
        }
    }
    KEnv::~KEnv(){
        if(!strcmp(ENV["REQUEST_METHOD"], "POST")) free(QUERY_STRING);
    }
    std::string KEnv::info(){
        std::string info; 
        info += "<div style='font-size: 13px; font-family: arial; padding: 5px; background: #ededed;'>";
        for ( int i = 0; i < nvs; i++ ){
            info += "<b style='color: #000000;'>ENV['";
            info += _ENV[i];
            info += "']</b> = "; 
            info += operator[](_ENV[i]);
            info += "</br>\n";
        }
        info += "</div>";
        return info;
    }
    void KEnv::header(const std::string& type){
        std::cout << "Content-type:" << type << "\r\n\r\n"; 
    }
    char* KEnv::operator[](std::string name){
        if (name == "QUERY_STRING")
            return QUERY_STRING;
        return ENV[name];
    }
    KEnv::operator std::string(){
        return this->info();
    }
}
#endif