#ifndef KPHP_CPP
#define KPHP_CPP

#include "kphp.h"

#include <utility>
namespace khuma{
    static const std::string base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

    static inline bool is_base64(unsigned char c) {
      return (isalnum(c) || (c == '+') || (c == '/'));
    }

    std::string base64_encode(const char * bytes_to_encode, unsigned int in_len) {
      std::string ret;
      int i = 0;
      int j = 0;
      unsigned char char_array_3[3];
      unsigned char char_array_4[4];

      while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
          char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
          char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
          char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
          char_array_4[3] = char_array_3[2] & 0x3f;

          for(i = 0; (i <4) ; i++)
            ret += base64_chars[char_array_4[i]];
          i = 0;
        }
      }

      if (i)
      {
        for(j = i; j < 3; j++)
          char_array_3[j] = '\0';

        char_array_4[0] = ( char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

        for (j = 0; (j < i + 1); j++)
          ret += base64_chars[char_array_4[j]];

        while((i++ < 3))
          ret += '=';

      }

      return ret;
    }

    std::string base64_decode(std::string const& encoded_string) {
      int in_len = encoded_string.size();
      int i = 0;
      int j = 0;
      int in_ = 0;
      unsigned char char_array_4[4], char_array_3[3];
      std::string ret;

      while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
        char_array_4[i++] = encoded_string[in_]; in_++;
        if (i ==4) {
          for (i = 0; i <4; i++)
            char_array_4[i] = base64_chars.find(char_array_4[i]);

          char_array_3[0] = ( char_array_4[0] << 2       ) + ((char_array_4[1] & 0x30) >> 4);
          char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
          char_array_3[2] = ((char_array_4[2] & 0x3) << 6) +   char_array_4[3];

          for (i = 0; (i < 3); i++)
            ret += char_array_3[i];
          i = 0;
        }
      }

      if (i) {
        for (j = 0; j < i; j++)
          char_array_4[j] = base64_chars.find(char_array_4[j]);

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

        for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
      }

      return ret;
    }
    
    std::string string_compress_encode(const std::string &data){
        std::stringstream compressed;
        std::stringstream original;
        original << data;
        boost::iostreams::filtering_streambuf<boost::iostreams::input> out;
        out.push(boost::iostreams::zlib_compressor());
        out.push(original);
        boost::iostreams::copy(out, compressed);

        std::string compressed_encoded = base64_encode(compressed.str().c_str(), compressed.str().size());

        return compressed_encoded;
    }

    std::string string_decompress_decode(const std::string &data){
        std::stringstream compressed_encoded;
        std::stringstream decompressed;
        compressed_encoded << data;

        std::stringstream compressed;
        compressed << base64_decode(compressed_encoded.str());
        
        boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
        in.push(boost::iostreams::zlib_decompressor());
        in.push(compressed);
        boost::iostreams::copy(in, decompressed);
        return decompressed.str();
    }


    size_t _strlen(const std::string& text){ 
        return text.size(); 
    }
    size_t _strlen(const char * text){
        size_t count = 0;
        for (; text[count] != '\0'; count++);
        return static_cast<int>(count);
    }
    int _strpos(const std::string& haystack, const std::string& needle){
        char *p = strstr(haystack.c_str(), needle.c_str());
        if (p != nullptr) {
            return p - haystack.c_str();
        }
        return -1;
    }
    std::string _replace(const std::string& search, const std::string& replace, const std::string& data){
        std::string newdata = data;
        newdata.replace(_strpos(data, search), _strlen(search), replace);
        return newdata;
    }
    std::string _substr(const std::string& data, int begin, int n){
        return data.substr(begin, n);
    }
    std::string _strreplace(const std::string& search, const std::string& replace, std::string data){
        std::string newdata = std::move(data);
        size_t pos = _strpos(newdata, search);
        auto size_replace = _strlen(replace);
        int current;
        while (pos != -1) {
            newdata.replace(pos, _strlen(search), replace);
            current = pos+size_replace;
            pos = _strpos(_substr(newdata, current, search.size()-(current)), search);
            if (pos != -1) { pos += current; }
        }
        return newdata;
    }
    std::vector<std::vector<std::string>> _preg(const std::string& pattern, std::string data){
        std::regex e(pattern.c_str());
        std::smatch _matches;
        auto match = std::regex_search(data, _matches, e);
        int msize = _matches.size();
        int len = 1;
        int i = 0;
        std::vector<std::vector<std::string>> _result;
        
        while (match) {
            for (size_t o = 0; o < msize; o++){
                _result[o][i] = _matches[o];
            }
            i++;
            size_t n   = _strpos(data, _matches[0]);
            data       = _substr(data, n+_strlen(_matches[0]), _strlen(data)-n);
            match      = std::regex_search(data, _matches, e);
        }
        return _result;
    }
    std::string _pregreplace(const std::string& pattern, const std::string& replace, const std::string& data){
        std::string _result = data;
        auto preg = _preg(pattern, data);
        for (size_t i = 0; preg[0][i] != "\0"; i++) {
            _result = _replace(preg[0][i], replace, _result);
        }
        return _result;
    }
    size_t _substrcount(const std::string& pattern, std::string data){
        size_t begin, finish, i;
        begin = i = 0;
        while ((finish = _strpos(data, pattern)) != -1) {
            auto cut = _substr(data, begin, finish);
            data = _substr(data, finish+1, _strlen(data)-begin);
            i++;
        }
        return i+1;
    }
    std::vector<std::string> _explode(const std::string& pattern, std::string data){
        auto n = _substrcount(pattern, data);
        std::vector<std::string> _result;
        size_t begin, finish;
        begin = 0;
        for(int i = 0; i < n; i++){
            finish = _strpos(data, pattern);
            std::stringstream cut { _substr(data, begin, finish) };
            _result.push_back(cut.str());
            data = _substr(data, finish+1, _strlen(data)-begin);
        }
        return _result;
    }
    std::vector<std::string> _filterarray(std::vector<std::string> data_explode, const std::string& pattern){
        std::vector<std::string> _result;
        for (const auto& value : data_explode){
            if(value != pattern){
                _result.push_back(value);
            }
        }
        return _result;
    }
    std::string _implode(const std::string& pattern, std::vector<std::string> data_explode){
        std::string data;
        for (const auto& value : data_explode){
            data.append(pattern);
            data.append(value);
        }
        if(!data.empty()) {
            data = _substr(data, _strlen(pattern), _strlen(data)-_strlen(pattern));
        }
        return data;
    }
    void _redirect(std::string route){
        //_print("HTTP/1.1 302\n");
        _print("Location: ", std::move(route),"\r\n\r\n");
    }
    std::string _urlencode(const std::string& str){
        std::string new_str;
        char c;
        int ic;
        const char* chars = str.c_str();
        char bufHex[10];
        int len = strlen(chars);

        for(int i=0;i<len;i++){
            c = chars[i];
            ic = c;
            // uncomment this if you want to encode spaces with +
            /*if (c==' ') new_str += '+';   
            else */if ((isalnum(c) != 0) || c == '-' || c == '_' || c == '.' || c == '~') { new_str += c;
            } else {
                sprintf(bufHex,"%X",c);
                if(ic < 16) { 
                    new_str += "%0"; 
                } else {
                    new_str += "%";
                }
                new_str += bufHex;
            }
        }
        return new_str;
    }

    std::string _urldecode(const std::string& str){
        std::string ret;
        char ch;
        int i, ii, len = str.length();
        
        for (i=0; i < len; i++){
            if(str[i] != '%'){
                if(str[i] == '+') {
                    ret += ' ';
                } else {
                    ret += str[i];
                }
            }else{
                sscanf(str.substr(i + 1, 2).c_str(), "%x", &ii);
                ch = static_cast<char>(ii);
                ret += ch;
                i = i + 2;
            }
        }
        return ret;
    }
}
#endif