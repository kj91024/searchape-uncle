cmake_minimum_required(VERSION 3.11)
project(insthagame)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

find_program( MEMORYCHECK_COMMAND valgrind)

include_directories(include)

add_executable( searchape.out main.cpp src/kphp.cpp src/kenv.cpp)
target_link_libraries(searchape.out pqxx pq boost_iostreams curl)


add_executable( searchape-cronjob.out update.cpp src/kphp.cpp src/kenv.cpp)
target_link_libraries(searchape-cronjob.out pqxx pq boost_iostreams curl)