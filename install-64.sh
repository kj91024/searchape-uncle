#CENTOS 7
yum update -y
sudo yum install epel-release -y

#yum install firewalld -y
#sudo systemctl enable firewalld
#sudo systemctl start firewalld
#sudo systemctl restart dbus
#sudo systemctl restart firewalld


#systemctl status iptables
#systemctl stop iptables
#systemctl mask iptables

#INSTALL NGINX
sudo yum install nginx -y
sudo yum install git -y

#log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
#                  '$status $body_bytes_sent "$http_referer" '
#                  '"$http_user_agent" "$http_x_forwarded_for"';

echo 'user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    access_log  /var/log/nginx/access.log  main;
	sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
}' >> /etc/nginx/nginx.conf


#sudo yum remove httpd

sudo systemctl start nginx

sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload

sudo systemctl enable nginx
sudo systemctl start nginx

# INSTALL PHP 7
sudo yum remove php-cli mod_php php-common
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum install php70w -y

#sudo yum remove php-cli mod_php php-common


# INSTALL ADMINER
curl -LO https://github.com/vrana/adminer/releases/download/v4.3.1/adminer-4.3.1.php
mv adminer-4.3.1.php adminer.php
chmod 777 /var/lib/php/session

# INSTALL POSTGRESQL


# INSTALL COMPILER
yum install clang -y
yum install gcc -y
yum install gcc-c++ -y

# INSTALL CMAKE
yum install cmake3 -y

# INSTALL JSON
git clone https://github.com/nlohmann/json.git
cd json
cmake3 .
make -j 4
make install

# INSTALL libpqxx
# yum install libpqxx -y
yum install libpqxx-devel -y

# INSTALL BOOSTSTREAM
yum install epel-release -y
yum install boost boost-thread boost-devel -y
yum install boost-iostreams -y


# INSTALL CURL
yum install curl-devel -y

