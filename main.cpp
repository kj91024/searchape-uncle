#include <iostream>
#include <cstdlib>


//Falta pasarlo por Release

using namespace std;

#include "khuma.h"

/*void ping(string &url){
	CURL *curl;
	//CURLM *multi_handle;
	CURLcode res;
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	//multi_handle = curl_multi_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 1); // CURLOPT_TIMEOUT_MS
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT_MS, 1);
		//curl_easy_setopt(curl, CURLOPT_RETURNTRANSFER, 0);
		//curl_easy_setopt(curl, CURLOPT_NOBODY, 1);

		//curl_multi_add_handle(multi_handle, curl);
		//res = curl_easy_perform(multi_handle);
		res = curl_easy_perform(curl);
	    
	    if(res == 0){
	      	_print("perfect ping:", curl_easy_strerror(res));
	    } else {
	    	_print("bad ping");
	    }
	    //curl_multi_cleanup(multi_handle);
	    curl_easy_cleanup(curl);
	}
}*/
#define RANK_BASIC 			0
#define RANK_NORMAL 		1
#define RANK_PREFER 		2
#define RANK_VERY_PREFER 	3

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp){
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}
void get_page(string &url, std::string &readBuffer){
	CURL *curl;
  	CURLcode res;
  	curl = curl_easy_init();
  	if(curl) {
    	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
	    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
	    res = curl_easy_perform(curl);
	    curl_easy_cleanup(curl);
	}
}
std::string getKey(){
	int r = rand() % 31;
    switch (r) {
      case 0: return "AIzaSyB8gQZeqcEvuHIQF6zesXziFAESVCx_U2g"; break;
      case 1: return "AIzaSyCmnn_zGQBFF5LEcmjX14tAEgV5phmhlKU"; break;
      case 2: return "AIzaSyDdEx51Pvl4_qrky6vNx1L1hV-SmA6kQsQ"; break;
      case 3: return "AIzaSyBwGVq5f4wGmy9jMqjZT68I_C0TefuVGo0"; break;
      case 4: return "AIzaSyC17taD6R3G7Yj5sp2KsEn5HfYFd83PSNs"; break;
      case 5: return "AIzaSyCiRJV43ySSLBh5-AVvU0HcKYNPXoXardU"; break;
      case 6: return "AIzaSyC5MU8ewjmDMhXhlEVZg0EBrJxXuvOJ6pU"; break;
      case 7: return "AIzaSyAitjQBTabsH_Lc4zJj9TWps9_Qnw57CUk"; break;
      case 8: return "AIzaSyAmMxQvVpCExCIvcdxlx0bbfBmcU8BmmGU"; break;
      case 9: return "AIzaSyA6fFsrNXAhs3DycB7VtX8UqVf8qBHKGNc"; break;
      case 10: return "AIzaSyA_-vflOEWTJpI4GgwVBnUUpTzwG9mTInE"; break;
      case 11: return "AIzaSyDimB_XN0ohYIacxt2PuiksCsa0dkPl9SE"; break;
      case 12: return "AIzaSyBVkCcAbTWf5bU5w-dsiuxymVu76F7YpTM"; break;
      case 13: return "AIzaSyA6yvVkxT1kb6j70J6pLO4_gsdzBl_VenI"; break;
      case 14: return "AIzaSyAa-QSo6t4VQo1diI7_Udqoa_iUE0OqoHE"; break;
      case 15: return "AIzaSyDW38Vh9hOPseYG2WACbH1HAtZgIQgs-14"; break;
      case 16: return "AIzaSyB94OCb0UUT7zPbpF_gAeP-scn_bSM7ctA"; break;
      case 17: return "AIzaSyCazfn5ls-yahBEOqpXNb3BLqFZBkLyXNg"; break;
      case 18: return "AIzaSyCtervpGO-WVR_6018klsvMBci4XgJO2r4"; break;   
      case 19: return "AIzaSyD_XjO3Pr0Rwbks5KO0OTduBdUMgYs5hkY"; break;
      case 20: return "AIzaSyBIzF_1H3-lmYfXLUrI-KbnmeQQdAQSEJo"; break;
      case 21: return "AIzaSyD_iwMfa7v2dpQZEPVLq5-t2TVabBSKCZ0"; break; 
      case 22: return "AIzaSyBq3CA-ZtrkaKz-GbY3ejh6Bl1fdx4rWxU"; break;  
      case 23: return "AIzaSyB96HcwLWGypWeTKqprP0KwKoYxjFSYpuU"; break;  
      case 24: return "AIzaSyBlHJXMm-IEZDlqWvl4O6mwGrV_XYoyOrY"; break;
      case 25: return "AIzaSyBLvYf3NlalIql8Eb7-P4NaRr1i6j4uUjU"; break;
      case 26: return "AIzaSyBXPV-VB0s2ajWCl2Yr9dvpOFsRGq8hh3k"; break;
      case 27: return "AIzaSyBmpkUrZx5Vo07e5CeGS-S8EKcUHJzwqWk"; break; 
      case 28: return "AIzaSyDCH5Gxxg3A58maZnqpibiDgOe3uYpEEwE"; break; 
      case 29: return "AIzaSyDmPoZ8x9HgJM-Lexn4VghJVCe8ans86t8"; break;
      case 30: return "AIzaSyA5wZ5lM8OnjuVp8HsjvM0trjEVkehbR6E"; break;
    }
    return "-";
}

void iprocess_query(PSQLDatabase &db, int rank, std::string &query){
	// Este funcion sirve para asignar una query a un slave para que pueda ejecutarlo.

	// Preguntamos si hay algún slave disponible con ese rango 
	result res;
	res = db.query("SELECT id_slave, count_processing_querys, state"
						  " FROM slaves "
						  " WHERE rank = "+to_string(rank)+ " AND state = -1 LIMIT 1");
	int id_slave = 0;
	int count_processing_querys = -1;
	int state;
	if(res.size() == 0){ // preguntar si hay algun slave dentro del rango que pueda ser usado
		res = db.query("SELECT id_slave, count_processing_querys, state"
						  " FROM slaves "
						  " WHERE rank = "+to_string(rank)+
						  " ORDER BY count_processing_querys ASC LIMIT 1");
						  	
		if(res.size() == 0){// preguntar si hay algun slave fuera del rango que pueda ser usado
			res = db.query("SELECT id_slave"
						  " FROM slaves "
						  " WHERE rank != "+to_string(rank)+" AND state != 1 ORDER BY count_processing_querys ASC LIMIT 1");
		}
	}
	if (res.size() == 1){
		id_slave = res[0][0].as<int>();
		count_processing_querys = res[0][1].as<int>();
		state = res[0][2].as<int>();
	}

	if (id_slave > 0){ // solo si hay un slave disponible
		// Actualizamos el total 'count_processing_querys' +1 para decir que 
		// tiene por procesar o estan en proceso de N numeros
		count_processing_querys++;
		db.update("slaves", { {"count_processing_querys", to_string(count_processing_querys)} },
						    { {"id_slave", to_string(id_slave)} });
		
		if (state == -1){ // verificamos si el estado esta en -1 que significa que esta 
			// esperando queris que peuda procesar y lo modificamos a 0 que significa que 
			// esta listo para procesar o esta procesando datos.
			db.update("slaves", { {"state", "0"} }, 
							    { {"id_slave", to_string(id_slave)} });	
		}
	}
	db.insert("processing_querys", {
		{"id_pq", "default"},
		{"id_slave", to_string(id_slave)},
		{"query", db.quote(query)},
		{"state", "-1"},
		{"date", "NOW()"}
	});
}

int main(int argc, char const *argv[]){
	srand (time(NULL));
	BENCHMARK_BEGIN("all", HIDE);

	//std::cout << "Content-type: text/json" << "\r\n\r\n"; 

	khuma::KEnv ENV;
	ENV.header("text/json");
	//_print((string)ENV);
	
	//Cloudflare es nuestra seguridad y el cacheo de los archivos

	if(!strcmp(ENV["REQUEST_METHOD"], "POST")){
		stringstream route { ENV["REQUEST_URI"] };
		auto routes = _explode("/", route.str());
		routes = _filterarray(routes);
		if (routes.size() == 2){
			if (!strcmp(routes[0].c_str(), "api")){
				if ( !strcmp(routes[1].c_str(), "v1.0.0")){
					stringstream request;
					request << ENV["QUERY_STRING"];
					//request << "action=getcolumn&column=main_page&q=rihanna";

					// Suggest
					// string request = "q=rih&type=web&action=suggest";

					// Search
			 		//string request = "q=suso&action=search";
			 		// string request = "q=rihannas&action=search";
			 		// string request = "q=rihannas&explicit=1&type=web&action=search";
			 		
			 		// First Insert (Google)
			 		//string request = "q=rihanna&action=first_insert&data=%5B%7B%22url%22%3A%22https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DlWA2pjMjpBs%22%2C%22title%22%3A%22%3Cb%3ERihanna%3C%2Fb%3E%20-%20%3Cb%3EDiamonds%3C%2Fb%3E%20-%20YouTube%22%2C%22content%22%3A%22Nov%208%2C%202012%20%3Cb%3E...%3C%2Fb%3E%20Get%20%3Cb%3ERihanna%26%2339%3Bs%3C%2Fb%3E%20eighth%20studio%20album%20ANTI%20now%3A%20Download%20on%20TIDAL%3A%20http%3A%2F%2Fsmarturl.it%2FdownloadANTI%20Stream%20on%20TIDAL%3A%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FDiamonds_%28Rihanna_song%29%22%2C%22title%22%3A%22%3Cb%3EDiamonds%3C%2Fb%3E%20%28%3Cb%3ERihanna%3C%2Fb%3E%20song%29%20-%20Wikipedia%22%2C%22content%22%3A%22%26quot%3B%3Cb%3EDiamonds%3C%2Fb%3E%26quot%3B%20is%20a%20song%20recorded%20by%20Barbadian%20singer%20%3Cb%3ERihanna%3C%2Fb%3E%20for%20her%20seventh%20studio%20album%2C%20Unapologetic%20%282012%29.%20It%20was%20written%20by%20Sia%20Furler%20together%20with%20its%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DnUD26iTq2p8%22%2C%22title%22%3A%22%3Cb%3ERihanna%20Diamonds%3C%2Fb%3E%20lyrics%20-%20YouTube%22%2C%22content%22%3A%22Sep%2026%2C%202012%20%3Cb%3E...%3C%2Fb%3E%20buy%20Diamonds%20%3A%20http%3A%2F%2Fsmarturl.it%2Fdiamondsit.%20...%20%3Cb%3ERihanna%20Diamonds%3C%2Fb%3E%20lyrics.%20Zita%20Bafort.%20Loading...%20Unsubscribe%20from%20Zita%20Bafort%3F%20Cancel%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fgenius.com%2FRihanna-diamonds-lyrics%22%2C%22title%22%3A%22%3Cb%3ERihanna%3C%2Fb%3E%20%E2%80%93%20%3Cb%3EDiamonds%3C%2Fb%3E%20Lyrics%20%7C%20Genius%20Lyrics%22%2C%22content%22%3A%22%3Cb%3EDiamonds%3C%2Fb%3E%20Lyrics%3A%20Shine%20bright%20like%20a%20%3Cb%3Ediamond%3C%2Fb%3E%20%2F%20Shine%20bright%20like%20a%20%3Cb%3Ediamond%3C%2Fb%3E%20%2F%20Find%20light%20in%20the%20beautiful%20sea%20%2F%20I%20choose%20to%20be%20happy%20%2F%20You%20and%20I%2C%20you%20and%20I%20%2F%20We%26%2339%3Bre%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D1WifEFI6eK8%22%2C%22title%22%3A%22%3Cb%3ERihanna%3C%2Fb%3E%20-%20%3Cb%3EDiamonds%3C%2Fb%3E%20%28Lyric%20Video%29%20-%20YouTube%22%2C%22content%22%3A%22Oct%2015%2C%202012%20%3Cb%3E...%3C%2Fb%3E%20Get%20%3Cb%3ERihanna%26%2339%3Bs%3C%2Fb%3E%20eighth%20studio%20album%20ANTI%20now%3A%20Download%20on%20TIDAL%3A%20http%3A%2F%2Fsmarturl.it%2FdownloadANTI%20Stream%20on%20TIDAL%3A%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fpagesix.com%2F2018%2F09%2F14%2Fhollywood-music-elite-descended-upon-rihannas-diamond-ball%2F%22%2C%22title%22%3A%22Hollywood%2C%20music%20elite%20descended%20upon%20%3Cb%3ERihanna%26%2339%3Bs%20Diamond%3C%2Fb%3E%20Ball%22%2C%22content%22%3A%22Sep%2014%2C%202018%20%3Cb%3E...%3C%2Fb%3E%20A%20night%20after%20hosting%20her%20racy%20Savage%20X%20Fenty%20Show%20in%20Brooklyn%20to%20close%20out%20New%20York%20Fashion%20Week%2C%20%3Cb%3ERihanna%3C%2Fb%3E%20was%20back%20on%20the%20scene%20to%20throw%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fde.wikipedia.org%2Fwiki%2FDiamonds_%28Rihanna-Lied%29%22%2C%22title%22%3A%22%3Cb%3EDiamonds%3C%2Fb%3E%20%28%3Cb%3ERihanna%3C%2Fb%3E-Lied%29%20%E2%80%93%20Wikipedia%22%2C%22content%22%3A%22%3Cb%3EDiamonds%3C%2Fb%3E%20ist%20ein%20Lied%20von%20Sia%20Furler%2C%20Benjamin%20Levin%2C%20Mikkel%20S.%20Eriksen%2C%20Tor%20Erik%20Hermansen%20aus%20dem%20Jahr%202012.%20Es%20wurde%20f%C3%BCr%20die%20aus%20Barbados%20stammende%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fwww.rollingstone.com%2Fmusic%2Fmusic-news%2Fissa-rae-childish-gambino-rihanna-diamond-ball-724529%2F%22%2C%22title%22%3A%22See%20Issa%20Rae%2C%20Childish%20Gambino%20Perform%20at%20%3Cb%3ERihanna%26%2339%3Bs%20Diamond%3C%2Fb%3E%20Ball%20...%22%2C%22content%22%3A%22Sep%2014%2C%202018%20%3Cb%3E...%3C%2Fb%3E%20%3Cb%3ERihanna%3C%2Fb%3E%20held%20her%20fourth%20annual%20%3Cb%3EDiamond%3C%2Fb%3E%20Ball%20Thursday%20night%20at%20New%20York%26%2339%3Bs%20Cipriani%20Wall%20Street%2C%20a%20fundraising%20gala%20that%20featured%20a%20performance%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fwww.letras.com%2Frihanna%2Fdiamonds%2F%22%2C%22title%22%3A%22%3Cb%3EDIAMONDS%3C%2Fb%3E%20-%20%3Cb%3ERihanna%3C%2Fb%3E%20-%20LETRAS.COM%22%2C%22content%22%3A%22%3Cb%3ERihanna%3C%2Fb%3E%20-%20%3Cb%3EDiamonds%3C%2Fb%3E%20%28Letras%20y%20canci%C3%B3n%20para%20escuchar%29%20-%20So%20shine%20bright%2C%20tonight%2C%20you%20and%20I%20%2F%20We%26%2339%3Bre%20beautiful%20like%20diamonds%20in%20the%20sky%20%2F%20Eye%20to%20eye%2C%20so%20alive%20%2F%20We%26%2339%3Bre%26nbsp%3B...%22%2C%22source%22%3A%22cse%22%7D%2C%7B%22url%22%3A%22https%3A%2F%2Fwww.vagalume.com.br%2Frihanna%2Fdiamonds.html%22%2C%22title%22%3A%22%3Cb%3EDiamonds%3C%2Fb%3E%20-%20%3Cb%3ERihanna%3C%2Fb%3E%20-%20VAGALUME%22%2C%22content%22%3A%22Letra%20e%20m%C3%BAsica%20de%20%E2%80%9C%3Cb%3EDiamonds%3C%2Fb%3E%E2%80%9C%20de%20%3Cb%3ERihanna%3C%2Fb%3E%20-%20You%26%2339%3Bre%20a%20shooting%20star%20I%20see%20%2F%20A%20vision%20of%20ecstasy%20%2F%20When%20you%20hold%20me%2C%20I%26%2339%3Bm%20alive%20%2F%20We%26%2339%3Bre%20like%20%3Cb%3Ediamonds%3C%2Fb%3E%20in%20the%20sky.%22%2C%22source%22%3A%22cse%22%7D%5D";
			 		
					// Update Columns
					//string request = "q=rihanna&action=ucolumn&column=page_1&data=%5B%7B%22id%22%3A0%2C%22url%22%3A%22https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DS8DnZK1h0Ew%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E-BASE%202018%20%7C%20Midnight%20Salute%20Show%20-%20YouTube%22%2C%22desc%22%3A%22Sep%2013%2C%202018%20%3Cb%3E...%3C%2Fb%3E%20This%20was%20%3Cb%3EQ%3C%2Fb%3E-BASE%20-%20The%20Final%20Mission.%20For%2015%20years%2C%20we%20raved%20from%20bunker%20to%20%5Cnbunker%2C%20creating%20everlasting%20moments%20with%20each%20new%20edition.%22%7D%2C%7B%22id%22%3A1%2C%22url%22%3A%22https%3A%2F%2Fwww.qthemusic.com%2Fq-gold%2F%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E%20Gold%20%E2%80%94%20%3Cb%3EQ%3C%2Fb%3E%20Magazine%22%2C%22desc%22%3A%22This%20month%2C%20the%20landmark%20album%20of%20the%20year%20was%20released%3A%20Now%20That%26%2339%3Bs%20What%20I%20Call%20%5CnMusic%21%20100.%20It%26%2339%3Bs%20the%20latest%20instalment%20of%20the%20hits%20series%20that%20began%20in%201983%20and%26nbsp%3B...%22%7D%2C%7B%22id%22%3A2%2C%22url%22%3A%22https%3A%2F%2Fwww.q-dance.com%2Fen%2Fvideos%2Fx-qlusive-holland-xxl-2018-or-livestream%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E-dance%20%7C%20X-Qlusive%20Holland%20XXL%202018%20%7C%20Livestream%22%2C%22desc%22%3A%22A%20night%20filled%20with%20ultimate%20Dutch%20craziness%20is%20coming%2C%20and%20you%20can%20be%20part%20of%20it%21%20%5CnWe%26%2339%3Bre%20streaming%20X-Qlusive%20Holland%20XXL%20LIVE%20all%20night%2C%20so%20make%20sure%20to%20put%20on%26nbsp%3B...%22%7D%2C%7B%22id%22%3A3%2C%22url%22%3A%22https%3A%2F%2Ftechcrunch.com%2F2018%2F08%2F07%2Fanonymous-vows-to-take-down-q%2F%22%2C%22title%22%3A%22Anonymous%20vows%20to%20take%20down%20%3Cb%3EQ%3C%2Fb%3E%20%7C%20TechCrunch%22%2C%22desc%22%3A%22Aug%207%2C%202018%20%3Cb%3E...%3C%2Fb%3E%20%3Cb%3EQ%3C%2Fb%3E%2C%20to%20the%20uninitiated%2C%20is%20a%204Chan%20poster%20who%20claims%20to%20be%20connected%20deep%20inside%20%5Cnthe%20US%20government.%20%3Cb%3EQ%3C%2Fb%3E%20claims%20to%20have%20high%20level%20clearance%20and%26nbsp%3B...%22%7D%2C%7B%22id%22%3A4%2C%22url%22%3A%22https%3A%2F%2Fwww.q-dance.com%2Fen%2Fvideos%2Fdefqon-1-2018-or-spitnoise%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E-dance%20%7C%20Defqon.1%202018%20%7C%20Spitnoise%22%2C%22desc%22%3A%22Follow%20%3Cb%3EQ%3C%2Fb%3E-dance%20online%3A%20Facebook%3A%20http%3A%2F%2Fwww.facebook.com%2FQdance%3B%20Instagram%3A%20%5Cnhttp%3A%2F%2Fwww.instagram.com%2FQ_dance%3B%20Twitter%3A%20http%3A%2F%2Fwww.twitter.com%2FQ_dance%26nbsp%3B...%22%7D%2C%7B%22id%22%3A5%2C%22url%22%3A%22https%3A%2F%2Fqcasinoandhotel.com%2Fdine%2Fq-sports-bar%2F%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E%20Sports%20Bar%20%7C%20%3Cb%3EQ%3C%2Fb%3E%20Casino%20and%20Hotel%22%2C%22desc%22%3A%22%3Cb%3EQ%3C%2Fb%3E%20Casino%26%2339%3Bs%20sports%20bar%20is%20the%20ultimate%20sports%20experience.%20We%20have%20high%20definition%20%5CnTVs%20throughout%20the%20entire%20seating%20area%20and%20an%20impressive%20selection%20of%20beer%20and%26nbsp%3B...%22%7D%2C%7B%22id%22%3A6%2C%22url%22%3A%22https%3A%2F%2Fwww.vikings.com%2Fvideo%2Fvi-q-test-adam-thielen%22%2C%22title%22%3A%22vI.%3Cb%3EQ%3C%2Fb%3E.%20Test%3A%20Adam%20Thielen%22%2C%22desc%22%3A%22The%20vI.%3Cb%3EQ%3C%2Fb%3E.%20Test%20is%20back%20this%20week%20on%20%26quot%3BVikings%20Connected%2C%26quot%3B%20with%20WR%20Adam%20Thielen%20%5Cnbeing%20pushed%20to%20the%20limited%20by%20Chris%20Hawkey.%22%7D%2C%7B%22id%22%3A7%2C%22url%22%3A%22https%3A%2F%2Fwww.af.mil%2FAbout-Us%2FBiographies%2FDisplay%2FArticle%2F108485%2Fgeneral-charles-q-brown-jr%2F%22%2C%22title%22%3A%22GENERAL%20CHARLES%20%3Cb%3EQ%3C%2Fb%3E.%20BROWN%20JR.%20%26gt%3B%20U.S.%20Air%20Force%20%26gt%3B%20Biography%20...%22%2C%22desc%22%3A%22Gen.%20Charles%20%3Cb%3EQ%3C%2Fb%3E.%20Brown%20Jr.%20is%20the%20Commander%2C%20Pacific%20Air%20Forces%3B%20Air%20Component%20%5CnCommander%20for%20U.S.%20Indo-Pacific%20Command%3B%20and%20Executive%20Director%2C%20Pacific%20Air%26nbsp%3B...%22%7D%2C%7B%22id%22%3A8%2C%22url%22%3A%22https%3A%2F%2Fwww.q-park.ie%2F%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E-Park%22%2C%22desc%22%3A%22With%20a%20%3Cb%3EQ%3C%2Fb%3E-Park%20Premier%20Card%2C%20you%20can%20get%20up%20to%2050%25%20off%20your%20parking%21%20You%20could%20%5Cnalso%20access%20Advantage%20Club%20for%20local%20and%20national%20discounts%20from%20Ireland%26%2339%3Bs%26nbsp%3B...%22%7D%2C%7B%22id%22%3A9%2C%22url%22%3A%22https%3A%2F%2Fwww.rollingstone.com%2Fmusic%2Fmusic-news%2Fjanet-jackson-q-tip-got-til-its-gone-731288%2F%22%2C%22title%22%3A%22Janet%20Jackson%20Taps%20%3Cb%3EQ%3C%2Fb%3E-Tip%20for%20Global%20Citizen%20Festival%20Performance%20...%22%2C%22desc%22%3A%221%20day%20ago%20%3Cb%3E...%3C%2Fb%3E%20Janet%20Jackson%20recruited%20%3Cb%3EQ%3C%2Fb%3E-Tip%20for%20a%20soulful%20live%20version%20of%20their%20collaborative%201997%20%5Cntrack%20%E2%80%9CGot%20%26%2339%3BTil%20It%26%2339%3Bs%20Gone%E2%80%9D%20on%20Saturday%20during%20the%202018%20Global%26nbsp%3B...%22%7D%2C%7B%22id%22%3A10%2C%22url%22%3A%22https%3A%2F%2Fwww.imdb.com%2Ftitle%2Ftt0084556%2F%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E%20%281982%29%20-%20IMDb%22%2C%22desc%22%3A%22Directed%20by%20Larry%20Cohen.%20With%20David%20Carradine%2C%20Michael%20Moriarty%2C%20Candy%20Clark%2C%20%5CnRichard%20Roundtree.%20NYPD%20detectives%20Shepard%20and%20Powell%20are%20working%20on%20a%26nbsp%3B...%22%7D%2C%7B%22id%22%3A11%2C%22url%22%3A%22https%3A%2F%2Fwww.theqarena.com%2F%22%2C%22title%22%3A%22Quicken%20Loans%20Arena%20Official%20Website%22%2C%22desc%22%3A%22The%20%3Cb%3EQ%3C%2Fb%3E%2C%20located%20in%20downtown%20Cleveland%2C%20is%20the%20premier%20sports%20and%20entertainment%20%5Cnfacility%20in%20Northeast%20Ohio%20and%20is%20home%20to%20the%20Cavaliers%2C%20Monsters%2C%20Gladiators%20and%5Cn%26nbsp%3B...%22%7D%2C%7B%22id%22%3A12%2C%22url%22%3A%22https%3A%2F%2Fwww.cnbc.com%2Fvideo%2F2018%2F06%2F15%2Fq-tip-talks-music-streaming.html%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E-Tip%20talks%20music%20streaming%22%2C%22desc%22%3A%22Jun%2015%2C%202018%20%3Cb%3E...%3C%2Fb%3E%20CNBC%26%2339%3Bs%20Jon%20Fortt%20speaks%20with%20%3Cb%3EQ%3C%2Fb%3E-Tip%20of%20A%20Tribe%20Called%20Quest%20about%20the%20future%20of%20%5Cnstreaming%20music.%22%7D%2C%7B%22id%22%3A13%2C%22url%22%3A%22https%3A%2F%2Fwww.bet.com%2Fvideo%2Frate-the-bars%2F2018%2Ffeaturing-don-q.html%22%2C%22title%22%3A%22Rate%20The%20Bars%20-%20Don%20%3Cb%3EQ%3C%2Fb%3E%22%2C%22desc%22%3A%22Don%20%3Cb%3EQ%3C%2Fb%3E%20Thought%20These%20Lines%20From%20Slick%20Rick%20Were%20%26quot%3BKinda%20Horrible%26quot%3B.%20Rate%20the%20%5CnBars.%20S3%20%7C%20Digital%20Original%20%7C%2005%3A33%20SUBSCRIBE.%20Aired%209-24-2018%20...until%20he%20found%20%5Cnout%26nbsp%3B...%22%7D%2C%7B%22id%22%3A14%2C%22url%22%3A%22https%3A%2F%2Fwww.kqed.org%2Fnews%2Fprogram%2Fqedup%2F%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E%26%2339%3Bed%20Up%20%7C%20KQED%20News%20%7C%20KQED%20Public%20Media%20for%20Northern%20CA%22%2C%22desc%22%3A%22%3Cb%3EQ%3C%2Fb%3E%26%2339%3Bed%20Up%20is%20a%20weekly%20podcast%20that%20delivers%20the%20best%20local%20news%20stories%20from%20KQED%20%5Cn...%20that%20are%20important%20to%20your%20community%20by%20listening%20to%20%3Cb%3EQ%3C%2Fb%3E%26%2339%3Bed%20Up%20every%20week.%22%7D%2C%7B%22id%22%3A15%2C%22url%22%3A%22https%3A%2F%2Freserve.com%2Fr%2Flillies-q-chicago%22%2C%22title%22%3A%22Lillie%26%2339%3Bs%20%3Cb%3EQ%3C%2Fb%3E%20-%20Reservations%20Available%20Through%20Reserve%22%2C%22desc%22%3A%22Lillie%26%2339%3Bs%20%3Cb%3EQ%3C%2Fb%3E%20offers%20upscale%20BBQ%2C%20plus%20signature%20sides%2C%20an%20extensive%20beer%20selection%2C%20%5Cnand%20moonshine%20cocktails.%20The%20rustic%20space%20has%20Southern%20bistro%20charm%20and%20a%20chic%5Cn%26nbsp%3B...%22%7D%2C%7B%22id%22%3A16%2C%22url%22%3A%22https%3A%2F%2Fwww.stormbowling.com%2Fson-q-2%22%2C%22title%22%3A%22SON%21%3Cb%3EQ%3C%2Fb%3E%22%2C%22desc%22%3A%22The%20Son%21%3Cb%3EQ%3C%2Fb%3E%20is%20the%20progeny%20of%20one%20of%20the%20most%20dominant%20shapes%20in%20the%20Storm%20%5Cnlineage%3A%20the%20Centripetal%E2%84%A2%20Core.%20Since%20its%20inception%2C%20the%20evolution%20of%20the%20design%20%5Cncan%20be%26nbsp%3B...%22%7D%2C%7B%22id%22%3A17%2C%22url%22%3A%22https%3A%2F%2Fdocs.angularjs.org%2Fapi%2Fng%2Fservice%2F%24q%22%2C%22title%22%3A%22AngularJS%3A%20API%3A%20%24%3Cb%3Eq%3C%2Fb%3E%22%2C%22desc%22%3A%22%24%3Cb%3Eq%3C%2Fb%3E%20can%20be%20used%20in%20two%20fashions%20---%20one%20which%20is%20more%20similar%20to%20Kris%20Kowal%26%2339%3Bs%20%3Cb%3EQ%3C%2Fb%3E%20or%20%5CnjQuery%26%2339%3Bs%20Deferred%20implementations%2C%20and%20the%20other%20which%20resembles%20ES6%26nbsp%3B...%22%7D%2C%7B%22id%22%3A18%2C%22url%22%3A%22https%3A%2F%2Fstore.q-dance.com%2Fen%2F%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E-dance%20-%20%3Cb%3EQ%3C%2Fb%3E-dance%22%2C%22desc%22%3A%22%3Cb%3EQ%3C%2Fb%3E-DANCE%20SUNGLASSES%20BLACK.%20%E2%82%AC9%2C95.%20%2B%20Add%20to%20cart.%20%E2%82%AC9%2C95.%20%2B.%20%3Cb%3EQ%3C%2Fb%3E-DANCE%20%3Cb%3EQ%3C%2Fb%3E-%5CnDANCE%20T-SHIRT%20PATTERN%20BLACK.%20%3Cb%3EQ%3C%2Fb%3E-DANCE%20T-SHIRT%20PATTERN%20BLACK.%22%7D%2C%7B%22id%22%3A19%2C%22url%22%3A%22https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FQ_source%22%2C%22title%22%3A%22%3Cb%3EQ%3C%2Fb%3E%20source%20-%20Wikipedia%22%2C%22desc%22%3A%22The%20%3Cb%3EQ%3C%2Fb%3E%20source%20is%20a%20hypothetical%20written%20collection%20of%20primarily%20Jesus%26%2339%3B%20sayings%20%28logia%5Cn%29.%20%3Cb%3EQ%3C%2Fb%3E%20is%20part%20of%20the%20common%20material%20found%20in%20the%20Gospels%20of%20Matthew%20and%20Luke%26nbsp%3B...%22%7D%5D";
					
					// Check - Chequeamos si ya hay imagenes y videos
					// string request = "q=rihanna&action=check";

					//Es necesario que encodifiquen la data enviada.
			 		if (request.str() == ""){
						_print("Error: QUERY_STRING esta basio");
						return 0;
					}
					//  all ingresa defrente a la base de datos sin confirmar nada solo inserta tal cual.
			 		
			 		std::string d = request.str();
					auto _request = _explode("&" ,d);
					std::map<std::string, std::string> POST;
					for (int i = 0; i < _request.size(); ++i){
						auto values = _explode("=", _request[i]); 
						POST[values[0]] = _urldecode(values[1]);
					}
					
					//string connection_string("host=91.218.231.13 port=5432 dbname=searchape user=searchape password=LanCenter_123");
				    PSQLDatabase db("localhost", "searchape", "searchape", "LanCenter_123"); 
				    
					if(POST["action"] == "suggest"){ // Listo
				    	json s;
						BENCHMARK_BEGIN("time_data", HIDE);
						result res = db.query("SELECT *, LENGTH(query) as size "
					    					  "FROM queries "
					    					  "WHERE query LIKE E'" + db.quote(POST["q"], false) + "%' "
					    					  " GROUP BY id_query ORDER BY size ASC LIMIT 10");
						vector<std::string> data;
						for (int i = 0; i < res.size(); ++i){
				    		data.push_back(res[i][1].as<std::string>());
				    	}

				    	s["suggest"] = data;
				    	s["time"] = BENCHMARK_FINISH("time_data", SECONDS);
					    _print( s.dump() );
					} else if(POST["action"] == "search"){ // Listo
				    	vector<std::string> entries_columns = {"images", "video", "widgets", "mean"};
				    	result res = db.simpleSelect("queries", {{"query", db.quote(POST["q"])}});
						json s;
						
						if (res.size() != 0){
							BENCHMARK_BEGIN("first_insert", HIDE);
							json data = json::parse(string_decompress_decode(res[0][2].as<std::string>()));
							s["main_page"] = data;
							for (int i = 3; i < 10; ++i){
								if (!res[0][i].is_null()){
									std::string d = "page_";
									d += '1'+(i-3);
									data = json::parse(string_decompress_decode(res[0][i].as<std::string>()));
									s[d] = data;
								}
							}
							/*for (int i = 0; i < entries_columns.size(); ++i){
								int zs = i+11;
								if(!res[0][zs].is_null()){
									s[ entries_columns[i] ] = json::parse(string_decompress_decode(res[0][zs].as<std::string>()));
								}
							}*/
							if(!res[0][11].is_null()) s[ "images" ] = json::parse(string_decompress_decode(res[0][11].as<std::string>()));
							if(!res[0][12].is_null()) s[ "video" ] = json::parse(string_decompress_decode(res[0][12].as<std::string>()));
							if(!res[0][13].is_null()) s[ "widgets" ] = json::parse(string_decompress_decode(res[0][13].as<std::string>()));
							s["time"] = BENCHMARK_FINISH("first_insert", SECONDS);
						}
						_print(s.dump());
					} else if(POST["action"] == "first_insert"){ // Listo
						// la data que me envian tiene que estar bien encriptada.
						BENCHMARK_BEGIN("first_insert", HIDE);
						if (POST.find("q") != POST.end() && POST["q"] != ""){
							result res = db.simpleSelect("queries", {{"query", db.quote(POST["q"])}});

							if (res.size() == 0){ // No existe en 'queries'
								res = db.insert("queries", {
									{"id_query", "default"},
									{"query", db.quote(POST["q"])},
									{"main_page", db.quote(string_compress_encode(POST["data"]))},
									{"date", "NOW()"}
								}, "id_query");

								iprocess_query(db, RANK_BASIC, POST["q"]);
							} /* no es necesario esta parte ya que solo queremos insertar si no existe

							else { // Si existe en 'queries'
								//preguntamos si esta en 'processing_querys'
								res = db.simpleSelect("processing_querys", {{"query", db.quote(POST["q"])}});

								if (res.size() == 0){ // si no existe insertar

								}
								iu_();
								res = db.insert("queries", {
									{"id_query", "default"},
									{"query", db.quote(POST["q"])},
									{"main_page", db.quote(string_compress_encode(POST["data"]))},
									{"date", "NOW()"}
								}, "id_query");
							}*/

						} else {
							_print("no hay query");
						}
						BENCHMARK_FINISH("first_insert");
					} else if(POST["action"] == "ucolumn"){ // Listo
						BENCHMARK_BEGIN("ucolumn", HIDE);
						if (POST.find("column") != POST.end()){
							result res = db.simpleSelect("queries", {{"query", db.quote(POST["q"])}});	
							if (res.size()){ // Existe
								int id_query = res[0][0].as<int>();
								db.update("queries", { {POST["column"], db.quote(string_compress_encode(POST["data"]))} },
													 { {"id_query", to_string(id_query)} });
								_print(POST["data"]);
								_print("Columna '",POST["column"],"' actualizada");
							} else {
								_print("Columna '",POST["column"],"' no se pudo actualizar porque no existe la query '",POST["q"],"'");
							}
						} else {
							_print("coloca el parametro column");
						}
						BENCHMARK_FINISH("ucolumn");
					} else if(POST["action"] == "getcolumn"){ // Listo
						BENCHMARK_BEGIN("getcolumn", HIDE);
						result res = db.simpleSelect("queries", {{"query", db.quote(POST["q"])}});
						if(res.size() == 1){
							int id_query = res[0][0].as<int>();
							if (POST.find("column") != POST.end()){
								int index;
								if(POST["column"] == "main_page")   { index = 2; }
								else if(POST["column"] == "page_1") { index = 3; }
								else if(POST["column"] == "page_2") { index = 4; }
								else if(POST["column"] == "page_3") { index = 5; }
								else if(POST["column"] == "page_4") { index = 6; }
								else if(POST["column"] == "page_5") { index = 7; }
								else if(POST["column"] == "page_6") { index = 8; } 
								else if(POST["column"] == "page_7") { index = 9; } 
								else if(POST["column"] == "date")   { index = 10; } 
								else if(POST["column"] == "images") { index = 11; } 
								else if(POST["column"] == "video")  { index = 12; } 
								else if(POST["column"] == "widgets"){ index = 13; }
								json data = json::parse(string_decompress_decode(res[0][index].as<std::string>()));
								_print(data.dump());
							} else {
								_print("coloca el parametro column");
							}
						}
						BENCHMARK_FINISH("getcolumn");
					
					} else if(POST["action"] == "csetoken"){ // Listo
						BENCHMARK_BEGIN("csetoken", HIDE);
						if (POST.find("url") != POST.end()){
							auto url = POST["url"];
							auto res = db.insert("cse", {
								{"id", "default"},
								{"url", db.quote(url)},
								{"date", "NOW()"}
							}, "id");
							int id = res[0][0].as<int>();
							_print("Inserción de CSE Token [", id, "]");
						} else {
							_print("Falta el parametro url");
						}
						BENCHMARK_FINISH("csetoken");
					
					} else if(POST["action"] == "getcse"){
						BENCHMARK_BEGIN("infoslave", HIDE);
						result res = db.query("SELECT url FROM cse ORDER BY id DESC LIMIT 1");	
						if (res.size() == 1){ // Existe
							auto url = res[0][0].as<std::string>();
							_print(url);
						} else {
							_print("No hay Token");
						}
						BENCHMARK_FINISH("infoslave");
					} else if(POST["action"] == "ilogs"){
						BENCHMARK_BEGIN("ilogs", HIDE);
						if (POST.find("error_code") != POST.end() && POST.find("obj") != POST.end() && POST.find("description") != POST.end()){
							auto error_code = POST["error_code"];
							auto obj = POST["obj"];
							auto description = POST["description"];
							db.insert("logs", {
								{"id_log", "default"},
								{"error_code", db.quote(error_code)},
								{"date", "NOW()"},
								{"obj", db.quote(obj)},
								{"description", db.quote(description)}
							});
						} else {
							_print("Falta algun parametro");
						}
						BENCHMARK_FINISH("ilogs");
					} else if(POST["action"] == "getlogs"){
						BENCHMARK_BEGIN("getlogs", HIDE);
						if(POST.find("id_log") != POST.end() && POST["id_log"] != ""){
							auto id_log = stoi(POST["id_log"]);
							result res = db.simpleSelect("logs", {{ "id_log", id_log }});
							if (res.size() == 1){
								json s;
								s["id_log"] = res[0][0].as<int>();
								s["error_code"] = res[0][0].as<int>();
								s["date"] = res[0][0].as<std::string>();
								s["obj"] = res[0][0].as<std::string>();
								s["description"] = res[0][0].as<std::string>();
								_print(s.dump());
							} else {
								_print(" No hay ningun log en id_log: ", id_log);
							}
						} else {
							_print("Falta el id_log");
						}
						BENCHMARK_FINISH("getlogs");
					
					} else if(POST["action"] == "yt"){ // Listo
						BENCHMARK_BEGIN("yt", HIDE);
						auto query = POST["query"];

						std::string api = "https://www.googleapis.com/youtube/v3/search?key=" + getKey() +
										  "&part=id&order=relevance&maxResults=30&q="+_urlencode(query)+
										  "&type=video";
						std::string data;
						get_page(api, data);
						json _data = json::parse(data);
						std::string _videoIds;
						std::string videoId;
						for (int i = 0; i < _data["items"].size(); ++i){
							_videoIds += _data["items"][i]["id"]["videoId"].get<std::string>();
							if (i != _data["items"].size()-1)
								_videoIds += ",";
						}
						api = "https://www.googleapis.com/youtube/v3/videos?id=" + _videoIds + 
							  "&key=" + getKey() +
                			  "&part=snippet,contentDetails,statistics";
                		data.clear();
                		get_page(api, data);
                		_data = json::parse(data);

                		json j;
                		json c = json::array();
                		json d = json::array();
                		json h = json::array();
                		json i = json::array();
                		json t = json::array();
                		for (int o = 0; o < _data["items"].size(); ++o){
                			c.push_back(_data["items"][o]["snippet"]["channelTitle"].get<std::string>());
                			d.push_back(_data["items"][o]["contentDetails"]["duration"].get<std::string>());
                			h.push_back(_data["items"][o]["statistics"]["viewCount"].get<std::string>());
                			i.push_back(_data["items"][o]["id"].get<std::string>());
                			t.push_back(_data["items"][o]["snippet"]["localized"]["title"].get<std::string>());
                		}
                		j["c"] = c;
                		j["d"] = d;
                		j["h"] = h;
                		j["i"] = i;
                		j["t"] = t;
                		_print(j.dump());

						BENCHMARK_FINISH("yt");
					} else if(POST["action"] == "finish_query"){
						BENCHMARK_BEGIN("finish_query", HIDE);
						if (POST.find("query") != POST.end()){
							auto query = POST["query"];
							if (POST.find("extern") != POST.end()){ // Slave Externos
								db.query(" DELETE FROM processing_querys"
										 " WHERE query = " + db.quote(query));
								_print("processing_querys Update desde extern");
							} else { // Slave de Casa
								// para el delete de query comprobamos que este en proceso y que exista
								result res = db.query(" SELECT slaves.id_slave, slaves.count_processing_querys "
													  " FROM processing_querys "
													  " JOIN slaves ON processing_querys.id_slave = slaves.id_slave"
													  " WHERE processing_querys.query = "+db.quote(query)+" AND processing_querys.state = 0");
								if (res.size() == 1){ 
									// actualizar slaves su count_processing_querys verificar si es 
									// igual a 0 entonces colocarlo a state -1 
									// ya que se va a actualizar slaves seria bueno enviar la cpu y la ram que tienen
									auto id_slave = res[0][0].as<int>();
									auto count = (res[0][1].as<int>())-1;
									if (count == 0){ // actualizar tambien el state del slave
										db.update("slaves", { {"count_processing_querys", to_string(count)}, {"state", db.quote("-1")} },
														    { {"id_slave", to_string(id_slave)} });
									} else { // solo actualizar el count_processing_querys
										db.update("slaves", { {"count_processing_querys", to_string(count)} },
														    { {"id_slave", to_string(id_slave)} });
									}

									// delete query en process_query
									db.query(" DELETE FROM processing_querys"
											 " WHERE query = " + db.quote(query));
									_print("Query '", query, "'Actualizado");
								} else {
									_print("Query no existe o no esta en el estado '0'(procesando query)");	
								}
							}
						} else {
							_print("Necesito que me envies el parametro query");
						}
						BENCHMARK_FINISH("finish_query");
					} else if(POST["action"] == "gettask"){
						BENCHMARK_BEGIN("gettask", HIDE);
						result res, _res;
						json querys = json::array();
						if (POST.find("ip") == POST.end()){ // Slave externos
							res = db.query(" SELECT processing_querys.id_slave, processing_querys.query"
										   " FROM processing_querys"
										   " WHERE processing_querys.state = 1 OR processing_querys.state = -1 LIMIT 10");
							if (res.size() > 0){
								for (int i = 0; i < res.size(); ++i){
									auto query = res[i][1].as<std::string>();
									querys.push_back(query);
									auto id_slave = res[i][0].as<int>();
									if (id_slave != 0){
										// Actualizamos al slave que estaba con esta carga para que se
										// pueda entender que ya no ahora tiene un trabajo menos que lo
										// esta realizando otro slave externo.
										_res = db.simpleSelect("slaves", {{"id_slave", to_string(id_slave)}});
										if (_res.size() == 1){ // Aqui hay problemas
											//auto id_slave = _res[0][0].as<int>();
											auto count = _res[0][4].as<int>()-1;
											if (count == 0) { // actualizar tambien el state del slave
												db.update("slaves", {{"count_processing_querys", to_string(count)}, {"state", "-1"}},
																	{{"id_slave", to_string(id_slave)}});
											} else { // solo actualizar el count_processing_querys
												db.update("slaves", {{"count_processing_querys", to_string(count)}},
																	{{"id_slave", to_string(id_slave)}});
											}
										}
									}
									// (Update)Avisamos que estamos empezando a procesar esta querys 
									// porque por algo esta pidiendo trabajo x] y el id_slave lo ponemos a 0 
									db.update("processing_querys", { {"id_slave", "0"}, {"state", "0"}, {"date", "NOW()"} },
																   { {"query", db.quote(query)} });
								}
							}
						} else { // Slave de casa
							auto ip = POST["ip"];
							res = db.query("SELECT processing_querys.query FROM slaves "
										   "JOIN processing_querys ON  slaves.id_slave = processing_querys.id_slave  "
										   "WHERE slaves.ip = " + db.quote(ip) + " AND processing_querys.state = -1 LIMIT 10");
							if (res.size() > 0){ // Si tiene tareas pendientes
								for (int i = 0; i < res.size(); ++i){
									auto query = res[i][0].as<std::string>();
									querys.push_back(query);
								
									// (Update)Avisamos que estamos empezando a procesar esta querys 
									// porque por algo esta pidiendo trabajo x]
									db.update("processing_querys", { {"state", "0"}, {"date", "NOW()"} },
																   { {"query", db.quote(query)} });
								}
							} else { // Si no tiene tareas pendientes ayudar a otros que si tienen tareas pendientes
								res = db.query(" SELECT processing_querys.id_slave, processing_querys.query"
											   " FROM processing_querys"
											   " WHERE processing_querys.state = 1 OR processing_querys.state = -1 LIMIT 10");
								
								querys = json::array();
								if (res.size() > 0){
									auto n = res.size();
									_res = db.simpleSelect("slaves", {{"ip", db.quote(ip)}});
									if (_res.size() > 0){
										auto _id_slave = _res[0][0].as<int>();
										auto _count = _res[0][4].as<int>()+n;
										db.update("slaves", {{"count_processing_querys", to_string(_count)}, {"state", "0"}},
															{{"ip", db.quote(ip)}});
										
										for (int i = 0; i < res.size(); ++i){
											auto query = res[i][1].as<std::string>();
											querys.push_back(query);
											auto id_slave = res[i][0].as<int>();
											if (id_slave != 0){
												// Actualizamos al slave que estaba con esta carga para que se
												// pueda entender que ahora ya no tiene un trabajo menos que lo
												// esta realizando otro slave externo.
												_res = db.simpleSelect("slaves", {{"id_slave", to_string(id_slave)}});
												if (_res.size() == 1){
													//auto id_slave = _res[0][0].as<int>();
													auto count = _res[0][4].as<int>()-1;
													if (count == 0) { // actualizar tambien el state del slave
														db.update("slaves", {{"count_processing_querys", to_string(count)}, {"state", "-1"}},
																			{{"id_slave", to_string(id_slave)}});
													} else { // solo actualizar el count_processing_querys
														db.update("slaves", {{"count_processing_querys", to_string(count)}},
																			{{"id_slave", to_string(id_slave)}});
													}
												}
											}
											// (Update)Avisamos que estamos empezando a procesar esta querys 
											// porque por algo esta pidiendo trabajo x] y el id_slave lo ponemos a 0
											db.update("processing_querys", { {"state", "0"}, {"id_slave", to_string(_id_slave)}, {"date", "NOW()"} },
																	   	   { {"query", db.quote(query)} });
										}
									}
								}
							}
						}
						_print(querys.dump());
						BENCHMARK_FINISH("gettask");
					} /*else if(POST["action"] == "getserver"){

					}*/ else if(POST["action"] == "forcetask"){ // Forzar trabajo

					} else if(POST["action"] == "infoslave"){
						BENCHMARK_BEGIN("infoslave", HIDE);
						if (POST.find("ip") != POST.end()){
							json r;
							auto ip  = POST["ip"];
							result res = db.simpleSelect("slaves", {{"ip", db.quote(ip)}});	
							if (res.size() > 0){ // Existe
								auto count = res[0][4].as<int>();
								auto state = res[0][6].as<int>();
								
								r["count_processing_querys"] = count;
								r["state"] = state;
							}
							_print(r.dump());
						} else {
							_print("IP is not defined");							
						}
						BENCHMARK_FINISH("infoslave");
					} else if(POST["action"] == "iuslaves"){
						BENCHMARK_BEGIN("iuslaves", HIDE);
						if (POST.find("ip") != POST.end() && 
							POST.find("cpu") != POST.end() && 
							POST.find("ram") != POST.end() && 
							//POST.find("state") != POST.end() && 
							POST.find("rank") != POST.end()){

							auto ip  = POST["ip"];
							auto cpu = POST["cpu"];
							auto ram = POST["ram"];
							auto last_update = POST["last_update"];
							auto rank  = POST["rank"];
							
							result res = db.simpleSelect("slaves", {{ "ip", db.quote(ip) }});
							
							if (POST.find("state") != POST.end()) { // Si existe 'state' en el POST
								auto state = POST["state"];
								if(res.size() == 1){ // Ya existe lo actualizo
									db.update("slaves", {
											{"cpu", cpu},
											{"ram", ram},
											{"last_update","NOW()"},
											{"state", state},
											{"rank", rank}
										},{ {"ip", db.quote(ip)} }
									);
									_print("Actualizado");
								} else { // No existe lo inserto
									db.insert("slaves", {
										{"id_slave", "default"},
										{"ip", db.quote(ip)},
										{"cpu", cpu},
										{"ram", ram},
										{"count_processing_querys", "0"},
										{"last_update","NOW()"},
										{"state", state},
										{"rank", rank}
									});
								 	_print("Insertado");
								}
							} else {
								if(res.size() == 1){ // Ya existe lo actualizo
									db.update("slaves", {
											{"cpu", cpu},
											{"ram", ram},
											{"last_update","NOW()"},
											{"rank", rank}
										},{ {"ip", db.quote(ip)} }
									);
									_print("Actualizado");
								}	
							}
							_print(1);	
						} else {
							//_print("Falta algun parametro");
							_print(0);
						}
						BENCHMARK_FINISH("iuslaves");
					} else {
						_print("Error: '",POST["action"],"' no detectada");
					}
					
				}	
			}
		}
	} else {
		_print(BENCHMARK_FINISH("all", SECONDS));
		return 0;
	}
	BENCHMARK_FINISH("all");
	return 0;
}



/*



*/