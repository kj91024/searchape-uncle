#ifndef KPHP_H
#define KPHP_H
#pragma once

#include <iostream>
#include <regex>
#include <vector>

#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/stream_buffer.hpp>
#include <boost/iostreams/filter/zlib.hpp>

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/stream.hpp>

#include <curl/curl.h>
namespace khuma{

    template<typename T>
    void _print(T v) {
      std::cout << v;
    }
    template<typename T, typename... Args>
    void _print(T first, Args... args) {
      std::cout << first;
      _print(args...);
    }
    template <typename T>
    int _count(T a){
      return a.size();
    }
    
    std::string base64_encode(const char * bytes_to_encode, unsigned int in_len);
    std::string base64_decode(std::string const& encoded_string);
    
    std::string string_compress_encode(const std::string &data);
    std::string string_decompress_decode(const std::string &data);

    size_t _strlen(const std::string& text);
    size_t _strlen(const char * text);
    int _strpos(const std::string& haystack, const std::string& needle);
    std::string _replace(const std::string& search, const std::string& replace, const std::string& data);
    std::string _substr(const std::string& data, int begin, int n);
    std::string _strreplace(const std::string& search, const std::string& replace, std::string data);
    std::vector<std::vector<std::string>> _preg(const std::string& pattern, std::string data);
    std::string _pregreplace(const std::string& pattern, const std::string& replace, const std::string& data);
    size_t _substrcount(const std::string& pattern, std::string data);
    size_t _count(std::vector<std::string> data);
    std::vector<std::string> _filterarray(std::vector<std::string> data_explode, const std::string& pattern = "\0");
    std::vector<std::string> _explode(const std::string& pattern, std::string data);
    std::string _implode(const std::string& pattern, std::vector<std::string> data_explode);
    void _redirect(std::string route);

    std::string _urlencode(const std::string & sSrc);
    std::string _urldecode(const std::string & sSrc);
}
#endif