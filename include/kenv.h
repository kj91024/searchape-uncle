#ifndef KENV_H
#define KENV_H

#include <iostream>
#include <map>
#include <string.h>

namespace khuma{
    class KEnv{
        protected:
        int nvs;
        std::string _ENV[26];
        std::map<std::string,std::string> _GET;
        std::map<std::string,std::string> _HEAD;
        std::map<std::string,std::string> _POST;
        std::map<std::string,std::string> _PUT;
        std::map<std::string,std::string> _DELETE;
        std::map<std::string,std::string> _CONNECT;
        std::map<std::string,std::string> _OPTIONS;
        std::map<std::string,std::string> _TRASE;
        std::map<std::string,std::string> _PATCH;

        public:
        std::map<std::string, char*> ENV;
        static bool a;
        char *QUERY_STRING;
        KEnv();
        ~KEnv();
        std::string info();
        void header(const std::string& type);

        char* operator[](std::string name);
        operator std::string();
    };
}
#endif

//> POSGINX posgrestSQL + cgi + nginx

