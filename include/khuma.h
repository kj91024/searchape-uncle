#include "kenv.h"
#include "kphp.h"

#include <curl/curl.h>
#include <pqxx/pqxx>
#include <nlohmann/json.hpp>
#include <map>
#include <exception>
#include <sys/time.h>
#include <string.h>

using namespace std;
using namespace khuma;
using namespace pqxx;
using namespace nlohmann;

#define HIDE false
#define SHOW true
#define SECONDS false
#define MILLISECONDS true

// Milliseconds to Seconds
inline double mtos(double milliseconds){
	return milliseconds/1000;
}
class Benchmark{
	struct timeval tiq, tfq;
    double tiempoq;
    bool st;
	public:
		Benchmark(bool st){
			this->st = st;
			tiempoq = 0;
			gettimeofday(&tiq, NULL);
		}
		double info(std::string name){
			gettimeofday(&tfq, NULL);  
			tiempoq = (tfq.tv_sec - tiq.tv_sec)*1000 + (tfq.tv_usec - tiq.tv_usec)/1000.0;        
			if (st) _print("<!--[Finished '", name,"' in ",tiempoq," ms]-->\n");
			return tiempoq;
		}
};
std::map<std::string, Benchmark*> mark;
inline void BENCHMARK_BEGIN(std::string name, bool st = SHOW){
	mark[name] = new Benchmark(st);
} 
inline double BENCHMARK_FINISH(std::string name, bool st = MILLISECONDS){
	auto time = (st) ? mark[name]->info(name) : mtos(mark[name]->info(name));
	delete mark[name];
	mark.erase(name);
	return time;
}

#define TRANSACTIONAL  		true
#define NONTRANSACTIONAL 	false

class PSQLDatabase{
	connection *con_t; 		// Connection transactional
	connection *con_nont; 	// Connection nontrasactional
	nontransaction *nont;
	result r;

	void errorInfo(std::string what, std::string query){
		_print("<div style='font-size: 13px; font-family: arial; padding: 5px; background: #f7e9e9; color: #a90a0a;'>");
	    _print("<b>Database Error: </b>", what, "<br>\n");
	    _print("<b>Query was: </b>", query, "<br>\n");
	    _print("</div>");
	}

	public:
	PSQLDatabase(std::string host, std::string dbname, std::string user, std::string password, std::string port = "5432"){
		BENCHMARK_BEGIN("connection_db", HIDE);
		try{
			string connection_string("host=" + host + " port= " + port + " dbname=" + dbname + " user=" + user + " password=" + password);
			con_t 	 = new connection(connection_string.c_str());
			con_nont = new connection(connection_string.c_str());
			nont = new nontransaction(*con_nont);
		} catch (const pqxx::sql_error &e){
		    errorInfo(e.what(), e.query());
		    std::cerr << "Database error: " << e.what() << std::endl
		        	  << "Query was: " << e.query() << std::endl;
		} catch (const std::exception &e){
		    std::cerr << "Error: " << e.what() << std::endl;
		}
		BENCHMARK_FINISH("connection_db");
	}
	result query(std::string sql, bool type = NONTRANSACTIONAL){
		try{
			if (type){
				work t(*con_t);
				r = t.exec(sql.c_str());
				t.commit();
			} else {
				r = nont->exec(sql.c_str());
			}
		} catch (const pqxx::sql_error &e){
		    errorInfo(e.what(), e.query());
		    std::cerr << "Database error: " << e.what() << std::endl
		        	  << "Query was: " << e.query() << std::endl;
		} catch (const std::exception &e){
		    std::cerr << "Error: " << e.what() << std::endl;
		}
		return r;
	}
	result simpleSelect( std::string table, json where, std::string select = "*", bool type = NONTRANSACTIONAL){
		std::string sql = "SELECT " + select +
						  " FROM " + table;
		for (auto it = where.begin(); it != where.end(); ++it){
			sql += (it == where.begin()) ? " WHERE " : " AND ";
			sql += it.key();
			sql += " = "; 
			sql += it.value();
		}
		r = this->query(sql, type);
		return r;
	}
	result UDQuery(int action, std::string table, json data, json where = json::array(), bool type = NONTRANSACTIONAL){
		return IUDQuery(action, table, data, "", where, type);
	}
	result insert(std::string table, json data, std::string returning = "", bool type = NONTRANSACTIONAL){
		return IUDQuery(1, table, data, returning, json::array(), type);
	}
	result update(std::string table, json data, json where = "null", bool type = NONTRANSACTIONAL){
		return UDQuery(2, table, data, where, type);
	}
	/*result delete(std::string table, json data, json where = json::array(), bool type = NONTRANSACTIONAL){
		return UDQuery(3, table, data, where,);
	}*/
	result IUDQuery(int action, std::string table, json data, std::string returning = "", json where = "null", bool type = NONTRANSACTIONAL){
		std::string sql;
		switch(action){
			case 1: //Insert
				{
					std::string into;
					std::string value;					
					for (auto it = data.begin(); it != data.end(); ++it){
						into  += (it != data.begin()) ? ", " : " ";
						into  += it.key();
						value += (it != data.begin()) ? ", " : " ";
						value += it.value();
					}
					sql += "INSERT INTO " + table +
						   " (" + into + ")"
						   " VALUES (" + value + ")";
				}
			break;
			case 2: //Update
				{
					std::string set;
					std::string _where;					
					for (auto it = data.begin(); it != data.end(); ++it){
						set  += (it != data.begin()) ? ", " : " ";
						set  += it.key();
						set  += " = ";
						set  += it.value();
					}
					for (auto it = where.begin(); it != where.end(); ++it){
						_where  += (it != where.begin()) ? ", " : " ";
						_where  += it.key();
						_where  += " = ";
						_where  += it.value();
					}
					sql += "UPDATE " + table +
					       " SET " + set +
						   " WHERE " + _where;
				}
			break;
			case 3: //Delete
				{

				}
			break;
		}
		if (returning != ""){
			sql += "RETURNING ";
			sql += returning;
		}
		r = this->query(sql, type);
		return r;
	}

	std::string quote(std::string text, bool st = true){
		//text = "suso\'s";
		text = _strreplace("'", "\\'", text.c_str());
		if (st) {
			text = '\''+text+'\''; 
			return 'E'+text;
		}
		//if (st) return 'E'+nont->quote(text);
		return text;
	}
	~PSQLDatabase(void){
		delete nont;
		delete con_t;
		delete con_nont;
	}		
};